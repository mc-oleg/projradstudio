//---------------------------------------------------------------------------


#pragma hdrstop

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tdm::FDQuery2AfterScroll(TDataSet *DataSet)
{
	   quSalaryHistory->Filter = quSalaryHistoryEMP_NO->FieldName +"="+quemployeeEMP_NO->Value;
}
//---------------------------------------------------------------------------
void __fastcall Tdm::DataModuleCreate(TObject *Sender)
{
 quEmployee->Open();
 quSalaryHistory->Open();
}
//---------------------------------------------------------------------------
