object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 414
  Width = 484
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=C:\Program Files\Firebird\Firebird_3_0\examples\empbuil' +
        'd\EMPLOYEE.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'DriverID=FB')
    Connected = True
    Left = 56
    Top = 104
  end
  object FDQuery1: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select * FROM JOB;')
    Left = 304
    Top = 176
  end
  object FDQuery2: TFDQuery
    Active = True
    AfterScroll = FDQuery2AfterScroll
    Connection = FDConnection1
    SQL.Strings = (
      'SELECT * FROM EMPLOYEE;')
    Left = 200
    Top = 232
  end
end
