//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Data.Bind.Grid.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <Fmx.Bind.Grid.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Grid.hpp>
#include <FMX.Grid.Style.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Types.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Edit.hpp>
#include <FMX.DateTimeCtrls.hpp>
#include <FMX.ListBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TBindSourceDB *BindSourceDB2;
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiCategoryList;
	TTabItem *tiProductList;
	TLayout *Layout1;
	TButton *buBackCategoryList;
	TLabel *Label1;
	TScrollBox *ScrollBox1;
	TGridLayout *glCategory;
	TLabel *Label2;
	TLayout *Layout2;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buCatalog;
	TButton *buCard;
	TButton *buFeedback;
	TButton *buContacts;
	TLayout *Layout3;
	TButton *buBackList;
	TLabel *Label3;
	TListView *lvProduct;
	TTabItem *tiProductItem;
	TGridPanelLayout *GridPanelLayout2;
	TLayout *Layout4;
	TLinkListControlToField *LinkListControlToField1;
	TScrollBox *ScrollBox2;
	TImage *imProductImage;
	TLabel *laProductName;
	TLabel *laProductPrice;
	TMemo *meProductNote;
	TButton *buBackItem;
	TLabel *Label4;
	TLinkPropertyToField *LinkPropertyToFieldText;
	TLinkPropertyToField *LinkPropertyToFieldText2;
	TLinkPropertyToField *LinkPropertyToFieldBitmap;
	TLinkControlToField *LinkControlToField1;
	TTabItem *tiFeedback;
	TLayout *Layout5;
	TButton *Button1;
	TLabel *Label5;
	TScrollBox *ScrollBox3;
	TEdit *edFeedbackPhone;
	TLabel *Label6;
	TLabel *Label7;
	TLabel *Label8;
	TMemo *meFeedbackNote;
	TLabel *Label9;
	TButton *buSendFeedback;
	TEdit *edFeedbackFIO;
	TEdit *edFeedbackEmail;
	TButton *buBusket;
	TButton *buQuikOrder;
	TTabItem *tiCard;
	TTabItem *tiCardProduct;
	TLayout *Layout6;
	TButton *Button2;
	TLabel *Label10;
	TLayout *Layout7;
	TButton *Button3;
	TLabel *Label11;
	TListView *ListView1;
	TButton *Button4;
	TListBox *ListBox1;
	TButton *Button5;
	TListBoxItem *ListBoxItem1;
	TListBoxItem *ListBoxItem2;
	TListBoxItem *ListBoxItem3;
	TListBoxItem *ListBoxItem4;
	TListBoxItem *ListBoxItem5;
	TListBoxItem *ListBoxItem6;
	TListBoxItem *ListBoxItem7;
	TListBoxItem *ListBoxItem8;
	TListBoxItem *ListBoxItem9;
	TEdit *Edit1;
	TEdit *Edit2;
	TEdit *Edit3;
	TEdit *Edit4;
	TEdit *Edit7;
	TEdit *Edit8;
	TEdit *Edit9;
	TSwitch *Switch1;
	TDateEdit *DateEdit1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buCatalogClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall glCategoryResize(TObject *Sender);
	void __fastcall lvProductClick(TObject *Sender);
	void __fastcall buBackItemClick(TObject *Sender);
	void __fastcall buBackListClick(TObject *Sender);
	void __fastcall buBackCategoryListClick(TObject *Sender);
	void __fastcall buSendFeedbackClick(TObject *Sender);
	void __fastcall buFeedbackClick(TObject *Sender);
	void __fastcall buBusketClick(TObject *Sender);
	void __fastcall buCardClick(TObject *Sender);
private:	// User declarations
public:
	void __fastcall CategoryCellOnClick(TObject* Sender);
	void ReloadCategoryList();	// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
