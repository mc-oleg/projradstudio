//---------------------------------------------------------------------------


#pragma hdrstop

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------

void Tdm::FeedbackIns(UnicodeString aFIO, UnicodeString aPhone, UnicodeString aEmail, UnicodeString aNote)
{
	spFeedbackIns->ParamByName("FIO")->Value= aFIO;
	spFeedbackIns->ParamByName("PHONE")->Value= aPhone;
	spFeedbackIns->ParamByName("EMAIL")->Value= aEmail;
	spFeedbackIns->ParamByName("NOTE")->Value= aNote;
	spFeedbackIns->ExecProc();

}

//---------------------------------------------------------------------------
int Tdm::CardNew()
{
	spCardNew->ExecProc();
    return spCardNew->ParamByName("ID")->Value;

}
void Tdm::CardUpd(int aID, UnicodeString aClientFIO, UnicodeString aClientTel, UnicodeString aClientEmail, UnicodeString aClientAddress, int aDeliveryMethod, double aDeliveryDate, long double aDeliveryAmount)
{
	spCardUpd->ParamByName("ID")->Value= aID;
	spCardUpd->ParamByName("CLIENT_FIO")->Value= aClientFIO;
	spCardUpd->ParamByName("CLIENT_TEL")->Value= aClientTel;
	spCardUpd->ParamByName("CLIENT_EMAIL")->Value= aClientEmail;
	spCardUpd->ParamByName("CLIENT_ADDRESS")->Value= aClientAddress;
	spCardUpd->ParamByName("DELIVERY_METHOD")->Value= aDeliveryMethod;
	spCardUpd->ParamByName("DELIVERY_DATE")->Value= aDeliveryDate;
	spCardUpd->ParamByName("DELIVERY_AMOUNT")->Value= aDeliveryAmount;
	spCardUpd->ExecProc();
}
//---------------------------------------------------------------------------
void Tdm::CardProductAppend(int aCardID, int aProductID)
{
  spCardProductAppend->ParamByName("CARD_ID")->Value = aCardID;
  spCardProductAppend->ParamByName("PRODUCT_ID")->Value = aProductID;
  spCardProductAppend->ExecProc();
}
//---------------------------------------------------------------------------
void Tdm::CardProductAppend(int aProductID)
{
	if (FCardID == -1) {
        FCardID = CardNew();
	}
	CardProductAppend(FCardID, aProductID);
}
//---------------------------------------------------------------------------
void Tdm::CardRefresh()
{
	quCard->Close();
	quCard->ParamByName("CARD_ID")->Value=FCardID;
	quCard->Open();
	quCardProduct->Close();
	quCardProduct->ParamByName("CARD_ID")->Value=FCardID;
	quCardProduct->Open();
}
//---------------------------------------------------------------------------
void __fastcall Tdm::quCategoryAfterScroll(TDataSet *DataSet)
{
	quProduct->Filter = quProductCATEGORY_ID->FieldName +" = "+quCategoryID->AsString;
}
//---------------------------------------------------------------------------
void __fastcall Tdm::DataModuleCreate(TObject *Sender)
{
	FDConnection1 -> Connected =true;
}
//---------------------------------------------------------------------------
void __fastcall Tdm::FDConnection1AfterConnect(TObject *Sender)
{
	quCategory->Open();
    quProduct->Open();
}
//---------------------------------------------------------------------------
