object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 405
  Width = 432
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\VD\Documents\projradstudio\labOnlineStore\ONLI' +
        'NESTORE.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    AfterConnect = FDConnection1AfterConnect
    Left = 32
    Top = 72
  end
  object quCategory: TFDQuery
    Active = True
    AfterScroll = quCategoryAfterScroll
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    category.id,'
      '    category.name,'
      '    category.image,'
      '    category.sort_index'
      'from category'
      'order by category.sort_index')
    Left = 8
    Top = 136
    object quCategoryID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quCategoryNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 70
    end
    object quCategoryIMAGE: TBlobField
      FieldName = 'IMAGE'
      Origin = 'IMAGE'
    end
    object quCategorySORT_INDEX: TIntegerField
      FieldName = 'SORT_INDEX'
      Origin = 'SORT_INDEX'
    end
  end
  object quProduct: TFDQuery
    Active = True
    Filtered = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    product.id,'
      '    product.category_id,'
      '    product.name,'
      '    product.price,'
      '    product.note,'
      '    product.image'
      'from product'
      'order by product.name')
    Left = 56
    Top = 136
    object quProductID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quProductCATEGORY_ID: TIntegerField
      FieldName = 'CATEGORY_ID'
      Origin = 'CATEGORY_ID'
      Required = True
    end
    object quProductNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 70
    end
    object quProductPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'PRICE'
      Required = True
    end
    object quProductNOTE: TWideStringField
      FieldName = 'NOTE'
      Origin = 'NOTE'
      Size = 4000
    end
    object quProductIMAGE: TBlobField
      FieldName = 'IMAGE'
      Origin = 'IMAGE'
    end
  end
  object spFeedbackIns: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'FEEDBACK_INS'
    Left = 144
    Top = 104
    ParamData = <
      item
        Position = 1
        Name = 'FIO'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 2
        Name = 'PHONE'
        DataType = ftWideString
        ParamType = ptInput
        Size = 15
      end
      item
        Position = 3
        Name = 'EMAIL'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 4
        Name = 'NOTE'
        DataType = ftWideString
        ParamType = ptInput
        Size = 4000
      end>
  end
  object quCard: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    card.id,'
      '    card.client_fio,'
      '    card.client_tel,'
      '    card.client_email,'
      '    card.client_address,'
      '    card.delivery_method,'
      '    card.delivery_date,'
      '    card.delivery_amount,'
      '    card.amount,'
      '    card.status,'
      '    card.status_comment'
      'from card'
      'where card.id =:card_id')
    Left = 224
    Top = 120
    ParamData = <
      item
        Name = 'CARD_ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object quCardProduct: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    card_product.id,'
      '    card_product.card_id,'
      '    card_product.product_id,'
      '    card_product.product_price,'
      '    card_product.quantity,'
      '    product.name,'
      '    product.price,'
      '    product.note,'
      '    product.image'
      'from product'
      
        '   inner join card_product on (product.id = card_product.product' +
        '_id)'
      'where card_product.card_id =:card_id'
      'order by product.name')
    Left = 224
    Top = 64
    ParamData = <
      item
        Name = 'CARD_ID'
        ParamType = ptInput
      end>
  end
  object spCardNew: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'CARD_NEW'
    Left = 344
    Top = 72
    ParamData = <
      item
        Position = 1
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptOutput
      end>
  end
  object spCardProductAppend: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'CARD_PRODUCT_APPEND'
    Left = 336
    Top = 144
    ParamData = <
      item
        Position = 1
        Name = 'CARD_ID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'PRODUCT_ID'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object spCardUpd: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'CARD_UPD'
    Left = 336
    Top = 216
    ParamData = <
      item
        Position = 1
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'CLIENT_FIO'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 3
        Name = 'CLIENT_TEL'
        DataType = ftWideString
        ParamType = ptInput
        Size = 15
      end
      item
        Position = 4
        Name = 'CLIENT_EMAIL'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 5
        Name = 'CLIENT_ADDRESS'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 6
        Name = 'DELIVERY_METHOD'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 7
        Name = 'DELIVERY_DATE'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Position = 8
        Name = 'DELIVERY_AMOUNT'
        DataType = ftFloat
        ParamType = ptInput
      end>
  end
end
