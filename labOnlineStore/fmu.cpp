//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
#include "fruCategory.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------
 //---------------------------------------------------------------------------
void __fastcall Tfm::CategoryCellOnClick(TObject* Sender)
{
	int xCategoryID = ((TControl *)Sender)->Tag;
	//
	TLocateOptions xLO;
	dm->quCategory->Locate(dm->quCategoryID->FieldName, xCategoryID, xLO);
	//
	tc->GotoVisibleTab(tiProductList->Index);
}
//---------------------------------------------------------------------------
void Tfm::ReloadCategoryList()
{
	dm->quCategory->First();
	while(!dm->quCategory->Eof) {
		TfrCategory *x = new TfrCategory(glCategory);
		x-> Parent =glCategory;
		x-> Align = TAlignLayout::Client;
		x-> Name = "frCategory"+IntToStr(dm->quCategoryID->Value);
		x-> la->Text = dm->quCategoryNAME->Value;
		x-> im->Bitmap->Assign(dm->quCategoryIMAGE);
		x-> Tag = dm->quCategoryID->Value;
		x-> OnClick = CategoryCellOnClick;
		dm->quCategory->Next();
	}
	glCategory->RecalcSize();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buCatalogClick(TObject *Sender)
{
  tc->GotoVisibleTab(tiCategoryList->Index) ;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormShow(TObject *Sender)
{
	ReloadCategoryList();
}







//---------------------------------------------------------------------------
void __fastcall Tfm::glCategoryResize(TObject *Sender)
{
   //������ ������
   float x = (glCategory->Width < 400)? glCategory->Width: glCategory->Width /2;
   glCategory-> ItemHeight = x;
   glCategory-> ItemWidth = x;
   // ������������ ������ ������, ����� ������ ����� ��� ������
   glCategory->Height =
   Ceil((glCategory->ComponentCount -1)/(glCategory->Width / glCategory->ItemWidth))
   * glCategory->ItemHeight;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lvProductClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiProductItem->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackItemClick(TObject *Sender)
{
 tc->GotoVisibleTab(tiProductList->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackListClick(TObject *Sender)
{
  tc->GotoVisibleTab(tiCategoryList->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackCategoryListClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------





void __fastcall Tfm::buSendFeedbackClick(TObject *Sender)
{
	dm->FeedbackIns (
	edFeedbackFIO->Text
	,edFeedbackPhone->Text
	,edFeedbackEmail->Text
	,meFeedbackNote->Text
	);
	ShowMessage("����� ���������!");

	tc->GotoVisibleTab(tiMenu->Index);


	edFeedbackFIO->Text="";
	edFeedbackPhone->Text= "" ;
	edFeedbackEmail->Text= "";
	meFeedbackNote->Text="" ;


}
//---------------------------------------------------------------------------

void __fastcall Tfm::buFeedbackClick(TObject *Sender)
{
 tc->GotoVisibleTab(tiFeedback->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBusketClick(TObject *Sender)
{
 tc->GotoVisibleTab(tiCard ->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buCardClick(TObject *Sender)
{
 	tc->GotoVisibleTab(tiCard ->Index);
}
//---------------------------------------------------------------------------

