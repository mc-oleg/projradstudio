//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buGroupsClick(TObject *Sender)
{
 	tc->GotoVisibleTab (tiGroupNumbers->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackGroupNumbersClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackListClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiGroupNumbers->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackInfoClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::glGroupNumbersResize(TObject *Sender)
{
   //������ ������
   float x = (glGroupNumbers->Width < 400)? glGroupNumbers->Width: glGroupNumbers->Width /2;
   glGroupNumbers -> ItemHeight = x;
   glGroupNumbers -> ItemWidth = x;
   // ������������ ������ ������, ����� ������ ����� ��� ������
   glGroupNumbers ->Height =
   Ceil((glGroupNumbers ->ComponentCount -1)/(glGroupNumbers->Width / glGroupNumbers->ItemWidth))
   * glGroupNumbers->ItemHeight;
}

//---------------------------------------------------------------------------

void __fastcall Tfm::lvListClickChange(TObject *Sender)
{
	tc->GotoVisibleTab(tiInfo->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
    tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------

void Tfm::ReloadCategoryList()
{
	dm->quGroups->First();
	while(!dm->quGroups->Eof) {
		TfrCategory *x = new TfrCategory(glGroupNumbers);
		x-> Parent =glGroupNumbers;
		x-> Align = TAlignLayout::Client;
		x-> Name = "frCategory"+IntToStr(dm->quGroupsID->Value);
		x-> la->Text = dm->quGroupsNAME->Value;
		x-> im->Bitmap->Assign(dm->quGroupsIMAGE);
		x-> Tag = dm->quGroupsID->Value;
		x-> OnClick = GroupsCellOnClick;
		dm->quGroups->Next();
	}
	glGroupNumbers->RecalcSize();
}
void __fastcall Tfm::FormShow(TObject *Sender)
{
	ReloadCategoryList();
}
//---------------------------------------------------------------------------

