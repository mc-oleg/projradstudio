object dm: Tdm
  OldCreateOrder = False
  Height = 320
  Width = 350
  object FDConnection: TFDConnection
    Params.Strings = (
      'Database=C:\Users\VD\Software\IBExpert\GROUPS'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 96
    Top = 56
  end
  object quGroups: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'select '
      '    groups.id,'
      '    groups.number,'
      '    groups.image'
      'from groups'
      'order by groups.id')
    Left = 144
    Top = 208
  end
  object quStudents: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'select '
      '    students.id,'
      '    students.groupnumber,'
      '    students.firstname,'
      '    students.lastname,'
      '    students.description,'
      '    students.image'
      'from students'
      'order by students.lastname')
    Left = 248
    Top = 184
  end
end
