//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiGroupNumbers;
	TTabItem *tiList;
	TTabItem *tiInfo;
	TLabel *Label1;
	TLayout *Layout1;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buGroups;
	TButton *buAbout;
	TButton *buContact;
	TButton *buFeedback;
	TLayout *Layout2;
	TScrollBox *ScrollBox1;
	TGridLayout *glGroupNumbers;
	TLabel *laGroupNumbers;
	TButton *buBackGroupNumbers;
	TLayout *Layout3;
	TListView *lvListClick;
	TButton *buBackList;
	TLabel *Label2;
	TLayout *Layout4;
	TScrollBox *ScrollBox2;
	TButton *buBackInfo;
	TLabel *Label3;
	TImage *imInfo;
	TLabel *laName;
	TLabel *laLastName;
	TMemo *meInfo;
	void __fastcall buGroupsClick(TObject *Sender);
	void __fastcall buBackGroupNumbersClick(TObject *Sender);
	void __fastcall buBackListClick(TObject *Sender);
	void __fastcall buBackInfoClick(TObject *Sender);
	void __fastcall glGroupNumbersResize(TObject *Sender);
	void __fastcall lvListClickChange(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:
	void __fastcall CategoryCellOnClick(TObject* Sender);
	void ReloadCategoryList();	// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
