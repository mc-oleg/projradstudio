//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.ImgList.hpp>
#include <System.ImageList.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Colors.hpp>
#include <FMX.ListBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tbMenu;
	TButton *buNewImage;
	TButton *buClear;
	TButton *BuNewIm;
	TToolBar *tbOptions;
	TToolBar *tbImage;
	TLayout *la;
	TButton *buDelClick;
	TButton *Button5;
	TButton *Button6;
	TButton *Button7;
	TButton *Button10;
	TTrackBar *trRotationChange;
	TButton *buSendToBack;
	TButton *buBringToFrontClick;
	TImageList *il;
	TGlyph *Glyph1;
	TSelection *Selection1;
	TSelection *Selection2;
	TGlyph *Glyph2;
	TSelection *Selection3;
	TRectangle *Rectangle1;
	TButton *buNewRect;
	TToolBar *ToolBar1;
	TTrackBar *trRectRadius;
	TColorComboBox *ColorComboBoxRect;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall SelectionAllMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall trRotationChangeChange(TObject *Sender);
	void __fastcall buDelClickClick(TObject *Sender);
	void __fastcall buBringToFrontClickClick(TObject *Sender);
	void __fastcall buSendToBackClick(TObject *Sender);
	void __fastcall buNewImageClick(TObject *Sender);
	void __fastcall buClearClick(TObject *Sender);
	void __fastcall buNewRectClick(TObject *Sender);
	void __fastcall ColorComboBoxRectChange(TObject *Sender);
	void __fastcall trRectRadiusChange(TObject *Sender);
private:	// User declarations
TSelection *FSel;
void SelectionAll (TObject *Sender);

public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
