//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	FSel = NULL;
	SelectionAll(la);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::SelectionAllMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y)
{
   SelectionAll(Sender);

}
//---------------------------------------------------------------------------
  void Tfm::SelectionAll (TObject *Sender)
  {
	  tbOptions->Visible =(FSel != NULL);

	 if (FSel != NULL) {
		 FSel->HideSelection = true;
	 }

	  FSel = dynamic_cast<TSelection*>(Sender);

	 if (FSel != NULL)
	 {
		 FSel->HideSelection = false;
	 }

	 tbOptions->Visible =(FSel != NULL);

	 if (tbOptions->Visible)
	  {
	     trRotationChange->Value=FSel->RotationAngle;
	  }


	}

void __fastcall Tfm::trRotationChangeChange(TObject *Sender)
{
  FSel -> RotationAngle = trRotationChange->Value;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDelClickClick(TObject *Sender)
{
	 FSel->DisposeOf();
	 FSel = NULL;
	 SelectionAll(la);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBringToFrontClickClick(TObject *Sender)
{
  FSel->BringToFront();
  FSel->Repaint();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buSendToBackClick(TObject *Sender)
{
   FSel ->  SendToBack();
   FSel->Repaint();

}
//---------------------------------------------------------------------------

void __fastcall Tfm::buNewImageClick(TObject *Sender)
{
 TSelection*x=new TSelection(la);
 x->Parent =la;
 x->GripSize=5;
 x->Width=50+Random(100);
 x->Height=50+Random(100);
 x->Position->X=Random(la->Width-x->Width);
 x->Position->Y=Random(la->Height-x->Height);
 x->RotationAngle=Random(100)-50;
 x->OnMouseDown=SelectionAllMouseDown;
 TGlyph*xGlyph = new TGlyph(x);
 xGlyph->Parent=x;
  xGlyph->Align=TAlignLayout::Client;
   xGlyph->Images=il;
	xGlyph->ImageIndex=Random(il->Count);
	 SelectionAll(x);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buClearClick(TObject *Sender)
{
SelectionAll(la);

for (int i=la->ControlsCount-1; i>=0;i--)

 {
if(dynamic_cast<TSelection*>(la->Controls->Items[i]))
{la->RemoveObject(i);}

}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buNewRectClick(TObject *Sender)
{
 TSelection*x=new TSelection(la);
 x->Parent =la;
 x->GripSize=5;
 x->Width=50+Random(100);
 x->Height=50+Random(100);
 x->Position->X=Random(la->Width-x->Width);
 x->Position->Y=Random(la->Height-x->Height);
 x->RotationAngle=Random(100)-50;
 x->OnMouseDown=SelectionAllMouseDown;
 TRectangle*xRectangle = new TRectangle(x);
 xRectangle->Parent=x;
 xRectangle->Align=TAlignLayout::Client;
 xRectangle->HitTest=false;
 xRectangle->XRadius=Random(50);
 xRectangle->YRadius=xRectangle->XRadius;
 xRectangle->Fill->Color=
 TAlphaColorF::Create(Random(256),Random(256),Random(256)).ToAlphaColor();


 SelectionAll(x);
}
//---------------------------------------------------------------------------




void __fastcall Tfm::ColorComboBoxRectChange(TObject *Sender)
{
 TRectangle*x=(TRectangle*)FSel->Controls->Items[0];
 x->Fill->Color=ColorComboBoxRect->Color;

}

//---------------------------------------------------------------------------


void __fastcall Tfm::trRectRadiusChange(TObject *Sender)
{
TRectangle*x=(TRectangle*)FSel->Controls->Items[0];
 x->XRadius = trRectRadius->Value;
 x->XRadius = x->XRadius;

}
//---------------------------------------------------------------------------

