//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.WebBrowser.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buBack;
	TButton *Stop;
	TButton *Reload;
	TButton *buForward;
	TToolBar *ToolBar2;
	TButton *buGo;
	TEdit *edURL;
	TWebBrowser *wb;
	void __fastcall buGoClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall buForwardClick(TObject *Sender);
	void __fastcall ReloadClick(TObject *Sender);
	void __fastcall StopClick(TObject *Sender);
	void __fastcall edURLKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall wbDidFinishLoad(TObject *ASender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
