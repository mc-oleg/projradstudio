//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TButton *buStart;
	TButton *buStop;
	TLabel *laTime;
	TTimer *tm;
	TMemo *me;
	TButton *Circle;
	TButton *BuPause;
	TButton *BuContinue;
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall buStopClick(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
	void __fastcall CircleClick(TObject *Sender);
	void __fastcall BuPauseClick(TObject *Sender);
	void __fastcall BuContinueClick(TObject *Sender);
private:    TDateTime FTimeStart;
			int FCount;
			TDateTime TimePass;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
