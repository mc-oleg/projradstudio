//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStartClick(TObject *Sender)
{ FCount = 0;
me->Lines->Clear();
FTimeStart = Now();
tm->Enabled = true;

}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStopClick(TObject *Sender)
{
  tm->Enabled = false;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tmTimer(TObject *Sender)
{
 UnicodeString x;
 DateTimeToString(x, L"hh.nn.ss.zzz", Now() -FTimeStart);
laTime->Text = x.Delete(x.Length() -2, 2);

}
//---------------------------------------------------------------------------
void __fastcall Tfm::CircleClick(TObject *Sender)
{
      FCount++;
me->Lines->Add("Krug"+IntToStr(FCount)+ "=" + laTime->Text);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::BuPauseClick(TObject *Sender)
{
	 TimePass = Now() - FTimeStart;
	 tm->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::BuContinueClick(TObject *Sender)
{

TimePass = Now();
tm->Enabled = true;

}
//---------------------------------------------------------------------------

