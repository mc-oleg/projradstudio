//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buSaveClick(TObject *Sender)
{
ta->Post();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buCloseClick(TObject *Sender)
{
ta->Cancel() ;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buSaveToFileClick(TObject *Sender)
{
ta->SaveToFile(Ioutils::TPath::GetDocumentsPath()+PathDelim+edFileName->Text+".xml");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buLoadFromFileClick(TObject *Sender)
{
 ta->LoadFromFile(Ioutils::TPath::GetDocumentsPath()+PathDelim+edFileName->Text+".xml");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buAddAge1Click(TObject *Sender)
{
ta->Edit() ;
taAge ->Value += 1;
ta->Post() ;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buAddAllMoney100Click(TObject *Sender)
{
 ta->First();
 while(! ta->Eof){
 ta->Edit() ;
 taMoney ->Value +=100;
 ta->Post();
 ta->Next();
 }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ckReadOnlyGridChange(TObject *Sender)
{
Grid1->ReadOnly=ckReadOnlyGrid->IsChecked;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ckReadOnlyTableChange(TObject *Sender)
{
ta->ReadOnly=ckReadOnlyTable->IsChecked;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buAplyClick(TObject *Sender)
{
	ta->Filter = Edit5->Text;
	ta->Filtered = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buExitClick(TObject *Sender)
{
	ta->Filtered = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buLocateClick(TObject *Sender)
{
	ta->Locate(taAge->FieldName, Edit6->Text);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buLookupClick(TObject *Sender)
{
    ShowMessage(ta->Lookup(taAge->FieldName, Edit6->Text, taFIO->FieldName));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buGetSumClick(TObject *Sender)
{
	Double xSum = 0;
	ta->DisableControls() ;
	TByteDynArray xBookmark = ta->GetBookmark();
	ta->First();
	while (! ta->Eof){
		xSum += taMoney->Value;
		ta->Next();
	}

	ta->GotoBookmark(xBookmark);
	ta->EnableControls();
    ShowMessage(FloatToStr(xSum));
}
//---------------------------------------------------------------------------

