//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Types.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TImage *ImFatCat;
	TFloatAnimation *FloatAnimation1;
	TFloatAnimation *FloatAnimation2;
	TLabel *TLabel;
	TFloatAnimation *FloatAnimationSch;
	TImage *ImgCat;
	TFloatAnimation *FloatAnimation3;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall ImFatCatClick(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
private:	// User declarations
public:	  int FCountFatCat;	// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
