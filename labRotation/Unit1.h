//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Ani.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TImage *Image1;
	TButton *bu4;
	TButton *bu3;
	TButton *bu2;
	TButton *bu1;
	TBitmapAnimation *BitmapAnimation1;
	TTimer *Timer1;
	TImage *Image2;
	TTrackBar *tr;
	TButton *bu6;
	TButton *bu5;
	TText *laRotation;
	TTrackBar *tr2;
	void __fastcall bu3Click(TObject *Sender);
	void __fastcall bu1Click(TObject *Sender);
	void __fastcall bu2Click(TObject *Sender);
	void __fastcall bu4Click(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall trChange(TObject *Sender);
	void __fastcall bu5Click(TObject *Sender);
	void __fastcall bu6Click(TObject *Sender);
	void __fastcall tr2Change(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
