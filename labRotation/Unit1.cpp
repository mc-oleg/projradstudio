//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;

//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall Tfm::bu3Click(TObject *Sender) {
	Image1->RotationAngle = 60;
}

// ---------------------------------------------------------------------------
void __fastcall Tfm::bu1Click(TObject *Sender) {
	Image1->RotationAngle = 0;
}

// ---------------------------------------------------------------------------
void __fastcall Tfm::bu2Click(TObject *Sender) {
	Image1->RotationAngle = 30;
}

// ---------------------------------------------------------------------------
void __fastcall Tfm::bu4Click(TObject *Sender) {
	Image1->RotationAngle = 90;
}

// ---------------------------------------------------------------------------
void __fastcall Tfm::Timer1Timer(TObject *Sender) {
	// Image1->RotationAngle = Image1->RotationAngle+10;
	 Image2->RotationAngle -= 10;
}

// ---------------------------------------------------------------------------
void __fastcall Tfm::trChange(TObject *Sender) {
	Image1->RotationAngle = tr->Value;
	Image2->RotationAngle = tr->Value * 9;
	laRotation->Text = FloatToStr(Image1->RotationAngle);
}

// ---------------------------------------------------------------------------
void __fastcall Tfm::bu5Click(TObject *Sender) {
	tr->Value += 10;
}

// ---------------------------------------------------------------------------
void __fastcall Tfm::bu6Click(TObject *Sender) {
	tr->Value -= 10;
}

// ---------------------------------------------------------------------------
void __fastcall Tfm::tr2Change(TObject *Sender) {
	Image1->Opacity = tr2->Value;
	Image2->Opacity = tr2->Value;
}
// ---------------------------------------------------------------------------
