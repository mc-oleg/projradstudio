//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buTranslateClick(TObject *Sender)
{
	RESTRequest1->Execute();
	RESTRequest2->Execute();

	ShowMessage(RESTResponse2->JSONValue->ToString());
	Memo2->Text = RESTResponse1->JSONValue->ToString();
	//TJSONArray *xArr;
	//xArr = (TJSONArray*) TJSONObject::ParseJSONValue(RESTResponse1->JSONValue->ToString());
   // Memo2->Text = AnsiDequotedStr(xArr->Items[0]->ToString(),'"');
}
//---------------------------------------------------------------------------
