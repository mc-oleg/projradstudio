//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buStartClick(TObject *Sender)
{
IdTCPServer->Active = True;
me->Lines->Add("Active = true");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buStopClick(TObject *Sender)
{
IdTCPServer->Active = false;
me->Lines->Add("Active = false");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::IdTCPServerConnect(TIdContext *AContext)
{
me->Lines->Add(Format("[%s] - Client connected",
ARRAYOFCONST((AContext->Connection->Socket->Binding->PeerIP))
));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::IdTCPServerDisconnect(TIdContext *AContext)
{
me->Lines->Add(Format("[%s] - Client disconnected",
ARRAYOFCONST((AContext->Connection->Socket->Binding->PeerIP))
));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::IdTCPServerExecute(TIdContext *AContext)
{


	UnicodeString x = AContext -> Connection->Socket->ReadLn();
	me->Lines->Add("Input = " +x);
//


if(x=="time"){
	AContext ->Connection->Socket->WriteLn(TimeToStr(Now()));
}

if(x=="str"){
	AContext ->Connection->Socket->WriteLn(edStr->Text,
	IndyTextEncoding_UTF8());
}

if(x=="faq"){
	AContext ->Connection->Socket->WriteLn("");
}

if(x=="image"){
TMemoryStream *x = new TMemoryStream();

try {
	im->Bitmap->SaveToStream(x);
	x->Seek(0,0);
	AContext ->Connection->Socket->Write(x->Size);
	AContext ->Connection->Socket->Write (x);
}


  __finally{
	  delete x;
  }

}
}
//---------------------------------------------------------------------------
