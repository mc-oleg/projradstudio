//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TTabControl *tc;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TTabItem *TabItem5;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buStart;
	TButton *Button2;
	TButton *buClose;
	TLabel *Label1;
	TGridPanelLayout *GridPanelLayout2;
	TImage *Image1;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TGridPanelLayout *GridPanelLayout3;
	TGridPanelLayout *GridPanelLayout4;
	TImage *Image6;
	TImage *Image7;
	TImage *Image11;
	TImage *Image12;
	TImage *Image13;
	TImage *Image14;
	TImage *Image15;
	TImage *Image16;
	TMemo *me;
	TStyleBook *StyleBook1;
	TButton *Button1;
	void __fastcall Image1Click(TObject *Sender);
	void __fastcall Image1MouseLeave(TObject *Sender);
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Image1MouseEnter(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall buCloseClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
