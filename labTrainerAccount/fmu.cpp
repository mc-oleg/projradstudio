//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
	 void Tfm::DoRestart()
	 {
	   FCountCorrect = 0;
	   FCountWrong = 0;
	   FQuad = 1;
	   DoContinue();
	 }
//---------------------------------------------------------------------------
	 void Tfm::DoContinue()
	 {
	   laCorrect->Text=Format(L"����� =%d", ARRAYOFCONST((FCountCorrect)));
	   laWrong->Text=Format(L"������� =%d", ARRAYOFCONST((FCountWrong)));
	   //
	   int xValue1 = Random(99999999999999999*FQuad*FQuad);
	   int xValue2 = Random(20*FQuad*FQuad);
	   int xSign = (Random(2) == 1) ? 1 :-1;
	   int xResult = xValue1 + xValue2;
	   int xResultNew = (Random(2) == 1) ? xResult : xResult + (Random(7) * xSign);
	   //
	   FAnswerCorrect = (xResult == xResultNew);
	   laCode->Text = Format("%d +%d =%d",
	   ARRAYOFCONST((xValue1, xValue2, xResultNew)));
	 }
//---------------------------------------------------------------------------
	 void Tfm::DoAnswer(bool aValue)
	 {
		(aValue == FAnswerCorrect) ?
		  FCountCorrect++ :  FCountWrong++ ;
		  DoContinue();


		  //(aValue == FAnswerCorrect) ?   fa->Enabled = True : fa->Enabled = False;

	 }


void __fastcall Tfm::FormCreate(TObject *Sender)
{
  	Randomize();
	DoRestart();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buRestartClick(TObject *Sender)
{
   DoRestart();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buYesClick(TObject *Sender)
{
	DoAnswer(true);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buNoClick(TObject *Sender)
{
   DoAnswer(false);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buQuadClick(TObject *Sender)
{
  (FQuad < 10) ? FQuad++: FQuad =1;
  buQuad->Text = FQuad;
}
//---------------------------------------------------------------------------

