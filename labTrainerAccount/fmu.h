//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Ani.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buRestart;
	TButton *buInfo;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buYes;
	TButton *buNo;
	TGridPanelLayout *GridPanelLayout2;
	TRectangle *Rectangle1;
	TLabel *laCorrect;
	TRectangle *Rectangle2;
	TLabel *laWrong;
	TLabel *Label3;
	TLabel *laCode;
	TStyleBook *StyleBook1;
	TButton *buQuad;
	TImage *Image1;
	TFloatAnimation *fa;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall buYesClick(TObject *Sender);
	void __fastcall buNoClick(TObject *Sender);
	void __fastcall buQuadClick(TObject *Sender);
private:
	 // User declarations
     int FQuad;
	 int FCountCorrect;
	 int FCountWrong;
	 bool FAnswerCorrect;
	 void DoRestart();
	 void DoContinue();
	 void DoAnswer(bool aValue);

public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
